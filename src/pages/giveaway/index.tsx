import React from "react";
import { NextPage } from "next";

import Layout from "@/../../src/components/other/Layout/Layout";
import Contacts from "@/../../src/components/pages/Challange/Challange";

import Head from "next/head";

const GiveawayPage: NextPage = () => {
  return (
    <Layout>
      <Head>
        <title>Rekberin - Challange</title>
      </Head>
      <Contacts />
    </Layout>
  );
};

export default GiveawayPage;
