import React, { FC, useEffect, useState } from "react";

import { motion } from "framer-motion";

import styles from "./ContactsSection.module.scss";

import InputSecondary from "../../../UI/Input/InputSecondary/InputSecondary";
import Button from "../../../UI/Button/Button";
import Arrow from "../../../other/Icons/Arrow";
import Phone from "../../../other/Icons/Phone";
import Mail from "../../../other/Icons/Mail";
import Geo from "../../../other/Icons/Geo";
import { API } from "@/store/api/api";
import Swal from "sweetalert2";
import { useRouter } from 'next/router'
import InputPrimary from "@/components/UI/Input/InputPrimary/InputPrimary";

const ContactsSection: FC = () => {
  const [inputData, seinputData] = useState<any>({ status: "Pengecekan" })
  const [datas, setDatas] = useState<any>([])
  const router = useRouter()


  const creatData = async () => {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < 5) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
      counter += 1;
    }


    const resCheckUpload = await API.storage.upload(inputData.bukti[0]);

    if (resCheckUpload.data) {

      const { data, error } = await API.service('laporan').create({
        ...inputData,
        id_laporan: `LAP-${result.toUpperCase()}`,
        bukti: [{
          fileName: resCheckUpload.data.fileName,
          url: resCheckUpload.data.url
        }]
      })

      Swal.fire({
        icon: 'success',
        title: 'Berhasil !',
        text: "Laporan Berhasil Dikirim Dan Akan Divalidasi Oleh Admin Terlebih Dahulu Sebelum Di Publikasi.",
        allowOutsideClick: false,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          router.reload()
        } else if (result.isDenied) {
          Swal.fire('Changes are not saved', '', 'info')
        }
      })

    }
  }

  const getData = async () => {
    const { data, error } = await API.service('laporan').find({
      sort: { _id: -1 },
    })

    setDatas(data);
  }

  useEffect(() => {
    getData();

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
    })

    Toast.fire({
      icon: 'warning',
      title: 'Fitur Sedang Dalam Pengembangan'
    })
  }, [])

  return (
    <motion.section
      initial={{ translateX: "200px", opacity: 0 }}
      whileInView={{ translateX: "0px", opacity: 1 }}
      viewport={{ once: true }}
      className={styles.section}
    >
      <div className={styles.form}>
        <div>
          <h4 className={styles.heading}>Laporan Penipuan</h4>
          {/* <b>Rules / Aturan</b>
          <ul>
            <li>7. Pengumuman giveaway menyusul di grup WhatsApp</li> <br />
          </ul> */}

          <b><i>Harap Masukan Data Dengan Benar !</i></b>
        </div>
        <form>
          <div>
            <InputPrimary title="Judul Laporan" placeholder="Judul Laporan" onChange={(e) => seinputData({ ...inputData, title: e.target.value })} />

            <InputPrimary title="Nama Facebook Pelapor" placeholder="Nama Facebook Pelapor" onChange={(e) => seinputData({ ...inputData, fb_pelapor: e.target.value })} />

            <InputPrimary title="Nomor WhatsApp Pelapor" placeholder="081xxxx" onChange={(e) => seinputData({ ...inputData, wa_pelapor: e.target.value })} />

            <InputPrimary title="Kronologi" placeholder="Kronologi" onChange={(e) => seinputData({ ...inputData, kronologi: e.target.value })} />

            <InputSecondary title="Nama Facebook Penipu" placeholder="Nama Facebook Penipu" onChange={(e) => seinputData({ ...inputData, fb_penipu: e.target.value })} />

            <InputSecondary title="Nomor WhatsApp Penipu" placeholder="081xxxx" onChange={(e) => seinputData({ ...inputData, wa_penipu: e.target.value })} />

            <div className="mb-3">
              <label className="form-label">Upload Bukti</label>
              {/* <img src={inputData.bukti} className="img-fluid" alt="..." /> */}
              <input className="form-control" type="file" id="formFile" onChange={(e) => seinputData({ ...inputData, bukti: e.target.files })} />
            </div>
            {/* <select className="form-select" aria-label="Default select example 2" onChange={(e) => seinputData({ ...inputData, barang: e.target.value })}>
              <option selected disabled>Pilih Barang...</option>
              <option value="Mukena">Mukena</option>
              <option value="Baju koko">Baju Koko</option>
              <option value="Baju anak 1 setel">Baju anak 1 setel</option>
              <option value="Beras 5kg">Beras 5kg</option>
              <option value="Mie instan 20 pcs">Mie instan 20 pcs</option>
              <option value="Uang tunai Rp 25.000">Uang tunai Rp 25.000</option>
              <option value="Pampers anak 1 ball">Pampers anak 1 ball</option>
            </select> */}

          </div>
          <Button primary onClick={creatData} type="button" disabled>
            Kirim <Arrow />
          </Button>
        </form>
      </div>
      <div className={styles.info}>
        <h5>Penipuan Terbaru</h5>
        {/* <ul> */}
        {
          datas.map((item: any, index: any) => {
            return (
              <>
                <div className="card mb-3" key={index + 1}>
                  <img src={item.bukti[0].url} width="200px" height="200px" className="card-img-top" alt="..." />
                  <div className="card-body">
                    <h5 className="card-title mt-2">{item.title} | {item.id_laporan}
                      {/* {
                        item.status === "Terverifikasi" ? 
                          `|  Terverifikasi` : item.status === "Pengecekan" ? 
                          `|  Pengecekan` : null
                      } */}
                    </h5>
                    {/* <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                    {/* <br /> */}
                    <a href="#" className="btn btn-primary">Lihat Detail ➡️</a>
                    {/* <p className="card-text"><small className="text-muted">Last updated 3 mins ago</small></p> */}
                  </div>
                </div>
              </>
            )
          })
        }
        {/* </ul> */}
      </div>
    </motion.section>
  );
};

export default ContactsSection;
