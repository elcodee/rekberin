import React, { FC } from "react";

import styles from "./Contacts.module.scss";
import ReportsSection from "./ReportSection/ReportSection";

const Report: FC = () => {
  return (
    <main className={styles.main}>
      <ReportsSection />
      {/* <QuestionsSection /> */}
    </main>
  );
};

export default Report;
