import React, { FC } from "react";

import styles from "./Footer.module.scss";
import Logo from "../../UI/Logo/Logo";
import Link from "next/link";

const Footer: FC = () => {
  return (
    <footer className={styles.footer}>
      <Logo />
      <center>
        <b>&copy; 2023 Rekberin Allrights Reserved <br /> Code By Elcode</b>
      </center>
      {/* <nav className={styles.navigation}>
        <ul>
          <li>
            <Link href="/" scroll={false}>
              Beranda
            </Link>
          </li>
          <li>
            <Link href="/" scroll={false}>
              Testimoni
            </Link>
          </li>
          <li>
            <Link href="/contacts/#header" scroll={false}>
              Kontak
            </Link>
          </li>
        </ul>
      </nav> */}
    </footer>
  );
};

export default Footer;
