import React, { FC } from "react";

import styles from "./PrimarySection.module.scss";
import Button from "../../../UI/Button/Button";

import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import { motion } from "framer-motion";

import Arrow from "../../../other/Icons/Arrow";

import Image from "next/image";
import { Autoplay } from "swiper";
import Link from "next/link";

import car from "/src/images/oversize/download.png";
import chart from "/src/images/icons/chart.png";
import fleet from "/src/images/icons/fleet.png";
import instructor from "/src/images/icons/instructor.png";
import discount from "/src/images/icons/discount.png";
import Swal from "sweetalert2";

const PrimarySection: FC = () => {
  return (
    <motion.section
      initial={{ translateY: "200px", opacity: 0 }}
      whileInView={{ translateY: "0px", opacity: 1 }}
      viewport={{ once: true }}
      className={styles.section}
    >
      <article className={styles.majorArticle}>
        <h1>Rekberin</h1>
        <p>
          Rekber aman dan terpcaya untuk nominal kecil maupun besar.
        </p>
        <Link href="/#form" scroll={false}>
          <Button primary>
            Rekber Sekarang <Arrow />
          </Button>
        </Link>
        <Link href="/giveaway" scroll={false}>
          <Button secondary>
            GiveAway <Arrow />
          </Button>
        </Link>
        {/* <Link href="#!" scroll={false}>
          <Button secondary onClick={() => {
            const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 3000,
              timerProgressBar: true,
            })

            Toast.fire({
              icon: 'warning',
              title: 'Fitur Sedang Dalam Pengembangan'
            })
          }}>
            Testimoni <Arrow />
          </Button>
        </Link> */}
        <Image src={car} alt="Rekberin" />
      </article>
      <div className={styles.items}>
        <article>
          <Image src={chart} alt="Flexibel" />
          <h4>Flexibel</h4>
          <p>Kapanpun & Dimanapun</p>
        </article>
        <article>
          <Image src={instructor} alt="Admin Terpecaya" />
          <h4>Admin Terpecaya</h4>
          <p>Dipastikan Aman 100% Dari Penipuan Apapun</p>
        </article>
        {/* <article>
          <Image src={discount} alt="Рассрочка и скидки" />
          <h4>Рассрочка и скидки</h4>
          <p>На 12 м. и скидки студентам</p>
        </article> */}
      </div>
      <Swiper
        slidesPerView={"auto"}
        spaceBetween={20}
        autoplay={{
          delay: 1500,
          disableOnInteraction: true
        }}
        modules={[Autoplay]}
        className={styles.swiper}
      >
        <SwiperSlide className={styles.slide}>
          <article>
            <Image src={chart} alt="Flexibel" />
            <h4>Flexibel</h4>
            <p>Kapanpun & Dimanapun</p>
          </article>
        </SwiperSlide>
        {/* <SwiperSlide className={styles.slide}>
          <article>
            <Image src={fleet} alt="Автопарк" />
            <h4>Автопарк</h4>
            <p>Ежегодно обновляется</p>
          </article>
        </SwiperSlide> */}
        <SwiperSlide className={styles.slide}>
          <article>
            <Image src={instructor} alt="Admin Terpecaya" />
            <h4>Admin Terpecaya</h4>
            <p>Dipastikan Aman 100% Dari Penipuan Apapun</p>
          </article>
        </SwiperSlide>
        {/* <SwiperSlide className={styles.slide}>
          <article>
            <Image src={discount} alt="Рассрочка и скидки" />
            <h4>Рассрочка и скидки</h4>
            <p>На 12 м. и скидки студентам</p>
          </article>
        </SwiperSlide> */}
      </Swiper>
    </motion.section>
  );
};

export default PrimarySection;
