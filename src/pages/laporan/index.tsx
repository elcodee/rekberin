import React from "react";
import { NextPage } from "next";

import Layout from "@/../../src/components/other/Layout/Layout";
import Report from "@/../../src/components/pages/Report/Report";

import Head from "next/head";

const ReportPage: NextPage = () => {
  return (
    <Layout>
      <Head>
        <title>Rekberin - Laporan Penipuan</title>
      </Head>
      <Report />
    </Layout>
  );
};

export default ReportPage;
