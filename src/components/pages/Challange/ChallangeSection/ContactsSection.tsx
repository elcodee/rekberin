import React, { FC, useEffect, useState } from "react";

import { motion } from "framer-motion";

import styles from "./ContactsSection.module.scss";

import InputSecondary from "../../../UI/Input/InputSecondary/InputSecondary";
import Button from "../../../UI/Button/Button";
import Arrow from "../../../other/Icons/Arrow";
import Phone from "../../../other/Icons/Phone";
import Mail from "../../../other/Icons/Mail";
import Geo from "../../../other/Icons/Geo";
import { API } from "@/store/api/api";
import Swal from "sweetalert2";
import { useRouter } from 'next/router'

const ContactsSection: FC = () => {
  const arraySelect = Array.from(Array(200), (item, index) => index + 1);
  const [inputData, seinputData] = useState<any>({})
  const [datas, setDatas] = useState<any>([])
  const [nomors, setNomors] = useState<any>([])
  const [pesertaTotal, setPesertaTotal] = useState<any>(0)
  const [err, setErr] = useState<any>({})
  const router = useRouter()

  const creatData = async () => {
    const resCheck1 = await API.service('peserta').find({
      where: { whatsapp: inputData.whatsapp },
    })

    const resCheck2 = await API.service('peserta').find({
      where: { nomor: inputData.nomor },
    })

    if (resCheck1.data && resCheck2.data) {
      if (resCheck2.data.length >= 0) {

        let restOfNum2: any = arraySelect.filter(function (item) {
          return !nomors.includes(item);
        })

        Swal.fire({
          icon: 'error',
          title: 'Gagal !',
          html: `<i>Nomor Undian <b>${inputData.nomor}</b> Sudah Terdaftar</i> <br /> <hr /> <br /> <b>Nomor Undian Tersedia</b> ${restOfNum2}`,
        })
      }

      if (resCheck1.data.length <= 0 && resCheck2.data.length <= 0) {
        const { data, error } = await API.service('peserta').create({
          name: inputData.name,
          whatsapp: inputData.whatsapp,
          nomor: parseInt(inputData.nomor),
          barang: inputData.barang,
        })

        Swal.fire({
          icon: 'success',
          title: 'Berhasil !',
          text: "Selamat, Semoga Beruntung :)",
        })

        router.reload()
      }
    }

  }

  const getData = async () => {
    let newArr: any = [];

    const resCount = await API.service('peserta').count()

    const { data, error } = await API.service('peserta').find({
      sort: { nomor: 1 },
    })
    
    setPesertaTotal(resCount?.data?.count)
    setDatas(data);

    data?.map((item: any) => {
      newArr.push(item.nomor)
    })

    setNomors(newArr)
  }

  useEffect(() => {
    getData();
  }, [])

  return (
    <motion.section
      initial={{ translateX: "200px", opacity: 0 }}
      whileInView={{ translateX: "0px", opacity: 1 }}
      viewport={{ once: true }}
      className={styles.section}
    >
      <div className={styles.form}>
        <div>
          <h4 className={styles.heading}>Give Away Lebaran Daget x Rekberin</h4>
          <b>Rules / Aturan</b>
          <ul>
            <li>1. Follow facebook admin (wajib) <br />
              - Nda / Baby : <a href="https://www.facebook.com/profile.php?id=100085017289863"><b>Klik Disini</b></a> <br />
              - Ram / Elcode : <a href="https://facebook.com/elcodeee"><b>Klik Disini</b></a>
            </li> <br />
            <li>2. Join grup <i>daget di whatsapp</i>, prioritas member digrup<br />
              Link Grup WhatsApp : <a href="https://chat.whatsapp.com/DyrXHGdIJG47ql7cQE38QV"><b>Join</b></a> </li> <br />
            <li>3. Pilih hadiah sesuai kebutuhan kalian.</li> <br />
            <li>4. Wajib isi form dengan benar</li> <br />
            <li>5. Ongkir ditanggung dengan kami</li> <br />
            <li>6. Pemenang di pilih total 5 orang</li> <br />
            <li>7. Pengumuman giveaway menyusul di grup WhatsApp</li> <br />
          </ul>

          <b><i>Nomor Undian Tidak Bisa Sama Satu Sama Lain Harus Berbeda.</i></b>
        </div>
        <form>
          <div>
            <InputSecondary title="Nama Facebook" placeholder="Nama Facebook" onChange={(e) => seinputData({ ...inputData, name: e.target.value })} />

            <InputSecondary title="Nomor WhatsApp" placeholder="081xxxx" onChange={(e) => seinputData({ ...inputData, whatsapp: e.target.value })} />

            <select className="form-select" aria-label="Default select example" onChange={(e) => seinputData({ ...inputData, nomor: e.target.value })}>
              <option selected disabled>Pilih Nomor Undian...</option>
              {
                arraySelect.map((item, index) => {
                  return (
                    <option value={item} key={index}>{item}</option>
                  )
                })
              }
            </select>

            <select className="form-select" aria-label="Default select example 2" onChange={(e) => seinputData({ ...inputData, barang: e.target.value })}>
              <option selected disabled>Pilih Barang...</option>
              <option value="Mukena">Mukena</option>
              <option value="Baju koko">Baju Koko</option>
              <option value="Baju anak 1 setel">Baju anak 1 setel</option>
              <option value="Beras 5kg">Beras 5kg</option>
              <option value="Mie instan 20 pcs">Mie instan 20 pcs</option>
              <option value="Uang tunai Rp 25.000">Uang tunai Rp 25.000</option>
              <option value="Pampers anak 1 ball">Pampers anak 1 ball</option>
            </select>

          </div>
          <Button primary onClick={creatData} type="button">
            Kirim <Arrow />
          </Button>
        </form>
      </div>
      <div className={styles.info}>
        <h5>Daftar Peserta ( {pesertaTotal} Orang )</h5>
        <ul>
          {
            datas.map((item: any, index: any) => {
              return (
                <li key={index}>{item.nomor}. {item.name} | {item.barang}</li>
              )
            })
          }
        </ul>
      </div>
    </motion.section>
  );
};

export default ContactsSection;
