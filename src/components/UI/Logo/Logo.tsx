import React, { FC } from "react";

import Image from "next/image";

import styles from "./Logo.module.scss";
import Link from "next/link";

import logo from "/public/fav.png";

const Logo: FC = () => {
  return (
    <Link href="/" scroll={false} className={styles.logo}>
      <Image src={logo} alt="Логотип" width={48} height={51} />
      <span className={styles.title}>Rekberin</span>
    </Link>
  );
};

export default Logo;
