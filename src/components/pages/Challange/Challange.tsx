import React, { FC } from "react";

import styles from "./Contacts.module.scss";
import ContactsSection from "./ChallangeSection/ContactsSection";

const Contacts: FC = () => {
  return (
    <main className={styles.main}>
      <ContactsSection />
      {/* <QuestionsSection /> */}
    </main>
  );
};

export default Contacts;
