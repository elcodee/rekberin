import React, { FC, useState } from "react";
import { motion } from "framer-motion";

import styles from "./FormSection.module.scss";
import Heading from "../../../UI/Heading/Heading";
import InputSecondary from "../../../UI/Input/InputSecondary/InputSecondary";
import Radio from "../../../UI/Radio/Radio";
import LongArrow from "../../../other/Icons/LongArrow";

const FormSection: FC = () => {
  const [inputData, setInputData] = useState<any>({});

  const clickRekber = async (formData: any) => {
    window.open(`https://api.whatsapp.com/send?phone=6285945798636&text=*Rekberin%20_NEW_*%0A%0ANama%20%3A%20${formData.nama}%0ABarang%20%3A%20${formData.barang}%0ANominal%20%3A%20${formData.nominal}%0A%0A_Saya%20setuju%20dengan%20syarat%20%26%20ketentuan%20*Rekberin*_`)
  }

  return (
    <motion.section
      initial={{ translateX: "-200px", opacity: 0 }}
      whileInView={{ translateX: "0px", opacity: 1 }}
      viewport={{ once: true }}
      className={styles.section}
      id="form"
    >
      <Heading>Mulai Rekber Sekarang</Heading>
      <form className={styles.form}>
        <div className={styles.inputs}>
          <InputSecondary
            required
            title="Nama"
            placeholder="Nama Lengkap"
            dark
            name="nama"
            onChange={(e) => setInputData({ ...inputData, nama: e.target.value })}
          />
          {/* <InputSecondary
            required
            title="Возраст"
            type="number"
            placeholder="Введите кол-во полных лет"
            name="age"
            dark
          /> */}
          <InputSecondary
            required
            title="Barang Rekber / Lainnya"
            type="text"
            placeholder="Akun, Baju dll..."
            dark
            name="barang"
            onChange={(e) => setInputData({ ...inputData, barang: e.target.value })}
          />
          {/* <InputSecondary
            required
            title="Электронная почта"
            type="email"
            placeholder="Введите вашу эл. почту"
            dark
            name="email"
          /> */}
        </div>
        <div className={styles.buttons}>
          <div>
            <h5>Nominal Rekber</h5>
            <div className={styles.category}>
              <Radio
                required
                title="0K - 99K (fee Gratis)"
                name="nominal"
                defaultValue="0K - 100K"
                dark
                onChange={(e) => setInputData({ ...inputData, nominal: e.target.value })}
              />
              <Radio
                required
                title="100k - 299k (fee 10k)"
                name="nominal"
                defaultValue="100K - 299K"
                dark
                onChange={(e) => setInputData({ ...inputData, nominal: e.target.value })}
              />
              <Radio
                required
                title="300K - 499K (fee 15k)"
                name="nominal"
                defaultValue="300K - 499K"
                dark
                onChange={(e) => setInputData({ ...inputData, nominal: e.target.value })}
              />
              <Radio
                required
                title="500K - 799K (fee 20k)"
                name="nominal"
                defaultValue="500K - 799K"
                dark
                onChange={(e) => setInputData({ ...inputData, nominal: e.target.value })}
              />
              <Radio
                required
                title="800K - 1 JT (fee 25k)"
                name="nominal"
                defaultValue="800K - 1 JT"
                dark
                onChange={(e) => setInputData({ ...inputData, nominal: e.target.value })}
              />
              <Radio
                required
                title="1 JT ++ (fee 5%)"
                name="nominal"
                defaultValue="1 JT ++"
                dark
                onChange={(e) => setInputData({ ...inputData, nominal: e.target.value })}
              />
            </div>
          </div>
          {/* <div>
            <h5>Предпочитаемая группа</h5>
            <div className={styles.group}>
              <Radio
                required
                title="Admin 1"
                name="wa_admin"
                defaultValue="081932709954"
                dark
              />
              <Radio
                required
                title="Admin 2"
                name="wa_admin"
                defaultValue="081932709954"
                dark
              />
            </div>
          </div> */}
        </div>
        {
          inputData.nama && inputData.barang && inputData.nominal &&
          <button onClick={() => clickRekber(inputData)} disabled={!inputData.nama || !inputData.barang || !inputData.nominal ? true : false} className={styles.submit}>
            Rekber <LongArrow />
          </button>
        }
      </form>
    </motion.section>
  );
};

export default FormSection;
