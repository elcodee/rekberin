import React, { FC } from "react";

import { motion } from "framer-motion";

import styles from "./ContactsSection.module.scss";

import InputSecondary from "../../../UI/Input/InputSecondary/InputSecondary";
import Button from "../../../UI/Button/Button";
import Arrow from "../../../other/Icons/Arrow";
import Phone from "../../../other/Icons/Phone";
import Mail from "../../../other/Icons/Mail";
import Facebook from "../../../other/Icons/FacebookIcon";
import Geo from "../../../other/Icons/Geo";

const ContactsSection: FC = () => {
  return (
    <motion.section
      initial={{ translateX: "200px", opacity: 0 }}
      whileInView={{ translateX: "0px", opacity: 1 }}
      viewport={{ once: true }}
      className={styles.section}
    >
      {/* <div className={styles.form}>
        <div>
          <h4 className={styles.heading}>Ada Pertanyaan?</h4>
          <p>
            Silahkan Kirim Pertanyaan anda kepada kami.
          </p>
        </div>
        <form>
          <div>
            <InputSecondary title="Nama" placeholder="Nama Lengkap" />
            <InputSecondary
              title="Pesan"
              type="tel"
              placeholder="Pesan"
            />
          </div>
          <Button primary>
            Tanya Sekarang <Arrow />
          </Button>
        </form>
      </div> */}
      <div className={styles.info}>
        <h5>Kontak</h5>
        <ul>
          <li>
            <span>
              <Phone /> <h6>WhatsApp</h6>
            </span>
            <span>
              <a href="https://api.whatsapp.com/send?phone=6285945798636&text=Halo%20Admin%20Rekberin">0859 4579 8636 {'(Admin 1)'}</a>
            </span>
            <span>
              <a href="https://api.whatsapp.com/send?phone=6281932709954&text=Halo%20Admin%20Rekberin">0819 3270 9954 {'(Admin 2)'}</a>
            </span>
          </li>
          <li>
            <span>
              <Facebook /> <h6>Facebook</h6>
            </span>
            <span>
              <a href="https://www.facebook.com/profile.php?id=100085017289863&mibextid=ZbWKwL">Ndaa</a>
            </span>
            <span>
              <a href="https://www.facebook.com/elcodeee?mibextid=ZbWKwL">Ram</a>
            </span>
          </li>
          <li>
            <span>
              <Geo /> <h6>Lokasi</h6>
            </span>
            <span>
              <a
                href="#!"
                rel="noreferrer"
                target="_blank"
              >
                Cimanggis Depok Jawa Barat, Indonesia
              </a>
            </span>
          </li>
        </ul>
      </div>
    </motion.section>
  );
};

export default ContactsSection;
