import React from "react";
import { NextPage } from "next";
import { Head, Html, Main, NextScript } from "next/document";

const Document: NextPage = () => {
  return (
    <Html lang="id">
      <Head>
        <link rel="icon" type="image" href="/fav.png" />
      </Head>
      <body>
        <Main />
        <NextScript />
        <div id="overlay"></div>
      </body>
    </Html>
  );
};

export default Document;
